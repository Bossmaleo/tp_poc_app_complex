package com.yamba.tpdevappcomplex

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.yamba.tpdevappcomplex.adapter.MyRecyclerViewAdapter
import com.yamba.tpdevappcomplex.databinding.ApparageFragmentBinding
import kotlinx.android.synthetic.main.apparage_fragment.*
import retrofit2.Response

class DisplayUser : AppCompatActivity() {

    private lateinit var binding : ApparageFragmentBinding
    private lateinit var adapter: MyRecyclerViewAdapter
    private lateinit var retservice : UserStopCovidService

    private val userListmaleo =  ArrayList<UserStopCovidItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.apparage_fragment)

        setSupportActionBar(toolbar)
        val actionbar = supportActionBar
        actionbar!!.title =  "Users Visibles"
        actionbar!!.setDisplayHomeAsUpEnabled(true)
        actionbar!!.setDisplayShowHomeEnabled(true)

        retservice = RetrofitInstance.getRetrofitInstance().create(UserStopCovidService::class.java)


        binding.userRecyclerview.layoutManager = LinearLayoutManager(applicationContext)
        adapter = MyRecyclerViewAdapter {
            {selectedItem: UserStopCovidItem ->listItemClicked(selectedItem)}
        }
        binding.userRecyclerview.adapter = adapter
        RequestDisplayUser()
    }


    override fun onOptionsItemSelected(item: MenuItem) : Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                val intent = Intent()
                setResult(Activity.RESULT_OK,intent)
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun RequestDisplayUser() {
        val responseLiveData: LiveData<Response<UserStopCovid>> = liveData {
            val response = retservice.getUsers()
            emit(response)
        }

        responseLiveData.observe(this, Observer {
            binding.progressbar.visibility = View.GONE
            val userList = it.body()?.listIterator()

            if(userList!=null){
                while (userList.hasNext()) {
                    userListmaleo.add(userList.next())
                    /*val userItem = userList.next()
                    Toast.makeText(activity,userItem.date_appairage,Toast.LENGTH_LONG).show()*/
                }


                //binding.progressbar.visibility = View.GONE

                adapter.setList(userListmaleo)
                adapter.notifyDataSetChanged()
            }

        })
    }

    private fun listItemClicked(user : UserStopCovidItem) {
        Toast.makeText(applicationContext,"Test", Toast.LENGTH_LONG).show()
        //Toast.makeText(applicationContext,"selected name is ${subscriber.name}",Toast.LENGTH_LONG).show()
        //subscriberViewModel.initUpdateAndDelete(subscriber)
    }

}