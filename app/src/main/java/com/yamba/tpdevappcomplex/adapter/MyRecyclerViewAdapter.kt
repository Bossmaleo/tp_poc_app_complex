package com.yamba.tpdevappcomplex.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.yamba.tpdevappcomplex.R
import com.yamba.tpdevappcomplex.UserStopCovidItem
import com.yamba.tpdevappcomplex.databinding.ListItemBinding


class MyRecyclerViewAdapter ( private val clickListener:(UserStopCovidItem)->Unit)
    : RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder>() {

    private val userList =  ArrayList<UserStopCovidItem>()

    class MyViewHolder(val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(user:UserStopCovidItem,clickListener:(UserStopCovidItem)->Unit){
            binding.userdId.text = user.id_user.toString()
            binding.userKeypush.text = user.keypush
            binding.monlayout.setOnClickListener{
                clickListener(user)
                //Toast.makeText(context,)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ListItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.list_item,parent,false)
        return MyViewHolder(binding)
    }

    /*fun setList(subscribers: List<User>){
        subscribersList.clear()
        subscribersList.addAll(subscribers)
    }*/

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(userList[position],clickListener)
    }

    fun setList(users: List<UserStopCovidItem>){
        userList.clear()
        userList.addAll(users)
    }
}