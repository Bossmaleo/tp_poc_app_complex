package com.yamba.tpdevappcomplex

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class WorkManager(context: Context, params: WorkerParameters) : Worker(context, params) {



    override fun doWork(): Result {

        try {

            /*val count = inputData.getInt(MainActivity.KEY_COUNT,0)*/

            for(i in 0..1000){
                Log.i("MYTAG","Uploading $i")
            }

            val finishedtime = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentData = finishedtime.format(Date())

            Log.i("MYTAG","Completed $currentData")

            /*val outputData = Data.Builder()
                .putString("KEY_WORKER",currentData)
                .build()*/

            return Result.success()

        }catch (e: Exception) {
            return Result.failure()
        }

    }



}