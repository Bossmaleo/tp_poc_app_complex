package com.yamba.tpdevappcomplex


import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.work.Data
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.yamba.tpdevappcomplex.databinding.ActivityMainBinding
import com.yamba.tpdevappcomplex.db.User
import com.yamba.tpdevappcomplex.db.UserDatabase
import com.yamba.tpdevappcomplex.db.UserRepository
import com.yamba.tpdevappcomplex.db.UserViewModelFactory
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var retservice : UserStopCovidService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        val dao = UserDatabase.getInstance(application).userDAO
        val repository = UserRepository(dao)
        val factory = UserViewModelFactory(repository)
        userViewModel = ViewModelProvider(this,factory).get(UserViewModel::class.java)

        retservice = RetrofitInstance.getRetrofitInstance().create(UserStopCovidService::class.java)


        userViewModel.user.observe(this, Observer {
            try {
                if(it.key_push!= null) {
                    //Toast.makeText(applicationContext, it.key_push, Toast.LENGTH_LONG).show()
                }
            }catch (e:Exception) {
                getKeyPushNotification()
            }


        })

        setPeriodicWorkRequest()


    }


    private fun getKeyPushNotification() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
                //on recupère le temps courant
                val time = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
                val currentDate = time.format(Date())
                if (token != null) {
                    RequestInsertUser(token,currentDate)
                }

                Log.i("MyTag",token)
            })
    }

    override fun onOptionsItemSelected(item: MenuItem) : Boolean {
        when(item.itemId) {
            R.id.menuliste -> {
                val intent = Intent(applicationContext,DisplayUser::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateOptionsMenu(menu: Menu) : Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main,menu)
        val icon_liste: Drawable = applicationContext.getDrawable(R.drawable.baseline_view_list_black_24)!!
        icon_liste!!.setColorFilter(applicationContext.getColor(android.R.color.white), PorterDuff.Mode.SRC_IN)
        menu.findItem(R.id.menuliste).icon = icon_liste
        return super.onCreateOptionsMenu(menu)
    }

   private fun RequestInsertUser(keypush: String,dateappairage : String) {
       val responseLiveData: LiveData<Response<SuccessResponse>> = liveData {
           val response = retservice.InsertUser(keypush,dateappairage,1)
           emit(response)
       }

       responseLiveData.observe(this, Observer {
           val userId = it.body()?.succes
           userViewModel.insert(User(0,userId.toString(),keypush,dateappairage))
           Toast.makeText(applicationContext,userId.toString(),Toast.LENGTH_LONG).show()
       })
    }

    private fun setPeriodicWorkRequest() {
        val periodicWorkRequest = PeriodicWorkRequest.Builder(com.yamba.tpdevappcomplex.WorkManager::class.java,15, TimeUnit.MINUTES)
            .build()
        WorkManager.getInstance(applicationContext).enqueue(periodicWorkRequest)
        WorkManager.getInstance(applicationContext).getWorkInfoByIdLiveData(periodicWorkRequest.id)
            .observe(this, Observer {
                if(it.state.isFinished) {
                    val data: Data = it.outputData
                    val message = data.getString("KEY_WORKER")
                    Toast.makeText(applicationContext,message,Toast.LENGTH_LONG).show()
                }
            })
    }

}
