package com.yamba.tpdevappcomplex

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface UserStopCovidService {

    @GET("colis/ColisApi/public/api/AffichageUserStopCovid")
    suspend fun getUsers() : Response<UserStopCovid>

    @GET("colis/ColisApi/public/api/InsertStoCovid")
    suspend fun InsertUser(@Query("id_keypush") keypush:String,@Query("date_apparage") date_apparage:String,@Query("etat") etat : Int) : Response<SuccessResponse>
}