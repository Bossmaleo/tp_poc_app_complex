package com.yamba.tpdevappcomplex

import com.google.gson.annotations.SerializedName

data class UserStopCovidItem(
    @SerializedName("date_appairage")
    val date_appairage: String,
    @SerializedName("id_user")
    val id_user: Int,
    @SerializedName("keypush")
    val keypush: String
)