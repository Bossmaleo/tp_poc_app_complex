package com.yamba.tpdevappcomplex.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "user_data_table")
class User (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_primary_key__name")
    var id : Int,
    @ColumnInfo(name = "user_id")
    var user_id : String,
    @ColumnInfo(name = "user_keypush")
    var key_push : String,
    @ColumnInfo(name = "date_apparage")
    var date_apparage : String

)