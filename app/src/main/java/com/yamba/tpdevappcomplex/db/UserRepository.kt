package com.yamba.tpdevappcomplex.db

import androidx.lifecycle.LiveData

class UserRepository(private val dao : UserDAO) {

    val user = dao.getUser()

    suspend fun insert(user: User) : Long {
        return dao.insertUser(user)
    }

    suspend fun update(user: User) : Int{
        return dao.updateUser(user)
    }

    suspend fun delete(user: User) : Int{
        return dao.deleteUser(user)
    }

    suspend fun deleteAll() : Int{
        return dao.deleteAll()
    }

    suspend fun getUser() : LiveData<User> {
        return dao.getUser()
    }

}