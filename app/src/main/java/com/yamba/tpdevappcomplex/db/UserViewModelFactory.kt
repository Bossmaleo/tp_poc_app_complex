package com.yamba.tpdevappcomplex.db

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yamba.tpdevappcomplex.UserViewModel
import java.lang.IllegalArgumentException

class UserViewModelFactory(private val repository: UserRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(UserViewModel::class.java)) {
            return UserViewModel(repository) as T
        }

        throw  IllegalArgumentException("Unknow View Model Class")
    }
}