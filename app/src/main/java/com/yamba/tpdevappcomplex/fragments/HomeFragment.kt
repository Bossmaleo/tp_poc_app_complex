package com.yamba.tpdevappcomplex.fragments

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.yamba.tpdevappcomplex.R
import com.yamba.tpdevappcomplex.databinding.HomeFragmentBinding


class HomeFragment : Fragment() {
    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)




         (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        val actionbar = (activity as AppCompatActivity?)!!.supportActionBar
        actionbar!!.title =  "StopCovid"
        actionbar!!.setDisplayHomeAsUpEnabled(false)
        actionbar!!.setDisplayShowHomeEnabled(true)

         binding.apparage.setOnClickListener {
             /*val bundle = bundleOf("user_input" to  binding.editText.text.toString())
             it.findNavController().navigate(R.id.action_homeFragment_to_secondFragment,bundle)*/
             findNavController().navigate(R.id.action_apparageFragment_to_apparageFragment)
         }
        return binding.root
    }





}
