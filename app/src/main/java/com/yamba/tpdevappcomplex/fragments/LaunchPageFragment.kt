package com.yamba.tpdevappcomplex.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.yamba.tpdevappcomplex.R
import com.yamba.tpdevappcomplex.databinding.LaunchpageFragmentBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class LaunchPageFragment : Fragment() {
    private lateinit var binding: LaunchpageFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.launchpage_fragment, container, false)
        //NavigationRoot()



        return binding.root
    }




    private fun NavigationRoot() {
        CoroutineScope(Dispatchers.Main) .launch {
            delay(100)
            //val bundle = bundleOf("user_input" to  binding.editText.text.toString())
            findNavController().navigate(R.id.action_launchpageFragment_to_homeFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        NavigationRoot()
    }
}