package com.yamba.tpdevappcomplex.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.yamba.tpdevappcomplex.*
import com.yamba.tpdevappcomplex.adapter.MyRecyclerViewAdapter
import com.yamba.tpdevappcomplex.databinding.ApparageFragmentBinding
import com.yamba.tpdevappcomplex.db.User
import retrofit2.Response

class ApparageFragmengt : Fragment() {
    private lateinit var binding: ApparageFragmentBinding
    private lateinit var adapter: MyRecyclerViewAdapter
    private lateinit var retservice : UserStopCovidService


    private val userListmaleo =  ArrayList<UserStopCovidItem>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.apparage_fragment, container, false)
        //NavigationRoot()

        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        val actionbar = (activity as AppCompatActivity?)!!.supportActionBar
        actionbar!!.title =  "Users Visibles"
        actionbar!!.setDisplayHomeAsUpEnabled(false)
        actionbar!!.setDisplayShowHomeEnabled(true)

        retservice = RetrofitInstance.getRetrofitInstance().create(UserStopCovidService::class.java)


        binding.userRecyclerview.layoutManager = LinearLayoutManager(activity)
        adapter = MyRecyclerViewAdapter {
            {selectedItem: UserStopCovidItem ->listItemClicked(selectedItem)}
        }
        binding.userRecyclerview.adapter = adapter
        RequestDisplayUser()
        return binding.root
    }

    private fun listItemClicked(user : UserStopCovidItem) {
        Toast.makeText(activity,"Test",Toast.LENGTH_LONG).show()
        //Toast.makeText(applicationContext,"selected name is ${subscriber.name}",Toast.LENGTH_LONG).show()
        //subscriberViewModel.initUpdateAndDelete(subscriber)
    }


    private fun RequestDisplayUser() {
        val responseLiveData: LiveData<Response<UserStopCovid>> = liveData {
            val response = retservice.getUsers()
            emit(response)
        }

        responseLiveData.observe(activity!!, Observer {
            val userList = it.body()?.listIterator()

            if(userList!=null){
                while (userList.hasNext()) {
                    userListmaleo.add(userList.next())
                    /*val userItem = userList.next()
                    Toast.makeText(activity,userItem.date_appairage,Toast.LENGTH_LONG).show()*/
                }


                //binding.progressbar.visibility = View.GONE

                adapter.setList(userListmaleo)
                adapter.notifyDataSetChanged()
            }

        })
    }
}