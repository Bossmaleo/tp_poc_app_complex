package com.yamba.tpdevappcomplex

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yamba.tpdevappcomplex.db.User
import com.yamba.tpdevappcomplex.db.UserRepository
import kotlinx.coroutines.launch

class UserViewModel(private val repository: UserRepository) : ViewModel(), Observable {

    val user = repository.user



    @Bindable
    val Keypush = MutableLiveData<String>()

    @Bindable
    val dateapparage = MutableLiveData<String>()








    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    fun insert(user: User)  =  viewModelScope.launch {
        val newRowId = repository.insert(user)
    }
}